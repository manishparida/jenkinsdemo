// GIT and Env Vars
def gitCreds = 'BitBucket'
def gitDeployRepo = 'https://manishparida@bitbucket.org/manishparida/terraform.git'
def gitBuildRepo = 'https://manishparida@bitbucket.org/manishparida/packer.git'
def gitBranch = 'master'
def packerDir = "packer"
def environment     = 'nonprod'

pipelineJob('Terraform-deploy') {
    description('Run terraform')
    logRotator(5, 5)
    parameters {
        choiceParam('gitCreds', [gitCreds], '')
        choiceParam('gitUrl', [gitDeployRepo], '')
        stringParam('gitBranch', gitBranch, 'Git branch name')
        choiceParam('instance', ["false", "true"], 'EC2 instance')
        choiceParam('sg', ["false", "true"], 'security group')
        choiceParam('terraformApplyPlan', ["false", "true"], 'terraform APPLY on above selected terraform plans')
    }
    definition {
        cps {
            script(readFileFromWorkspace('pipeline/deploy.groovy'))
            sandbox()
        }
    }
}

// Build Pipeline.
pipelineJob('windows2016-AMI') {
    description('')
    logRotator(5, 5)
    parameters {
        choiceParam('gitCreds', [gitCreds], '')
        choiceParam('gitUrl', [gitBuildRepo], '')
        stringParam('gitBranch', 'master', 'Git branch name')
        choiceParam('packerDir', [packerDir], '')
        choiceParam('packerTemplateFile', ['ami_template.json'], '')
        choiceParam('validate', ["false", "true"], 'AMI Packer Validate')
        choiceParam('build', ["false", "true"], 'AMI Packer Build')
    }
    definition {
        cps {
            script(readFileFromWorkspace('pipeline/ami_build.groovy'))
            sandbox()
        }
    }
}

pipelineJob('terraform-deploy-application') {
    description('application deploy pipeline')
    logRotator(10, 10)
    parameters {
        choiceParam('gitCreds', [gitCreds], '')
        choiceParam('gitUrl', [gitDeployRepo], '')
        stringParam('gitBranch', 'master', 'Git branch name')
        choiceParam('terraformenv', ["dev"], '')
        choiceParam('terraformaccounttype', ["nonprod"], 'The account type the job will run against')
        choiceParam('terraformLogLevel', ['','ERROR', 'WARN', 'INFO', 'DEBUG'], '')
        choiceParam('terraformBucket', ["manish-${environment}-tfstate"], '')
        choiceParam('terraformKey', ['default-vpc/test/'], '')
        choiceParam('terraformIncludeComputeALL', ['false','true'], 'true: also plan and apply ALB & ASG module<br>\
        false: runs nothing')
        choiceParam('terraformApplyPlan', ['false','true'], 'true: run a terraform plan and apply<br>\
        false: run a terraform plan only')
    }
    definition {
        cps {
            script(readFileFromWorkspace('pipeline/application.groovy'))
            sandbox()
        }
    }
}

pipelineJob('terraform-deploy-shared') {
    description('Creates common resources such as security groups.')
    logRotator(10, 10)
    parameters {
        choiceParam('gitCreds', [gitCreds], '')
        choiceParam('gitUrl', [gitDeployRepo], '')
        stringParam('gitBranch', 'master', 'Git branch name')
        choiceParam('terraformenv', ["dev"], '')
        choiceParam('terraformaccounttype', ["nonprod"], 'The account type the job will run against')
        choiceParam('terraformBucket', ["manish-${environment}-tfstate"], '')
        choiceParam('terraformPrefix', ['default-vpc/test'], '')
        choiceParam('terraformIncludeNetwork', ['false','true'], 'true: also plan and apply network module<br>\
		  false: runs only plan')
        choiceParam('terraformIncludeStorage', ['false','true'], 'true: also plan and apply storage module<br>\
		  false: runs only plan')
        choiceParam('terraformIncludeSNS', ['false','true'], 'true: also plan and apply sns module<br>\
		  false: runs only plan')
        choiceParam('terraformApplyPlan', ['false','true'], 'true: run a terraform plan & terraform apply<br>\
		  false: run a terraform plan only')
    }
    definition {
        cps {
            script(readFileFromWorkspace('pipeline/shared.groovy'))
            sandbox()
        }
    }
}

pipelineJob('terraform-deploy-RDS') {
    description('RDS deploy pipeline')
    logRotator(10, 10)
    parameters {
        choiceParam('gitCreds', [gitCreds], '')
        choiceParam('gitUrl', [gitDeployRepo], '')
        stringParam('gitBranch', 'master', 'Git branch name')
        choiceParam('terraformenv', ["dev"], '')
        choiceParam('terraformaccounttype', ["nonprod"], 'The account type the job will run against')
        choiceParam('terraformLogLevel', ['','ERROR', 'WARN', 'INFO', 'DEBUG'], '')
        choiceParam('terraformBucket', ["manish-${environment}-tfstate"], '')
        choiceParam('terraformKey', ['default-vpc/test/'], '')
        choiceParam('terraformIncludeRDS', ['false','true'], 'true: also plan and apply RDS module<br>\
        false: runs nothing')
        choiceParam('terraformApplyPlan', ['false','true'], 'true: run a terraform plan and apply<br>\
        false: run a terraform plan only')
    }
    definition {
        cps {
            script(readFileFromWorkspace('pipeline/rds.groovy'))
            sandbox()
        }
    }
}