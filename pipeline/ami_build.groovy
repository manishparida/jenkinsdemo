import groovy.json.JsonSlurper

node('master') {
    // Checkout git repository on local Jenkins platform
    stage ('Checkout') {
        deleteDir()
        checkout([$class: 'GitSCM', branches: [[name: gitBranch]], clearWorkspace: true, doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'SubmoduleOption', disableSubmodules: false, parentCredentials: true, recursiveSubmodules: true, reference: '', trackingSubmodules: false]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: gitCreds, url: gitUrl]]])
    }
    if (validate == 'true') {
    stage ('Validating') {
       bat "packer validate ${packerDir}/${packerTemplateFile}"
    }
    }
    if (build == 'true') {
        withCredentials([[$class           : 'AmazonWebServicesCredentialsBinding',
                          credentialsId    : 'AWSCred',
                          accessKeyVariable: 'AWS_ACCESS_KEY_ID',
                          secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
    // Bake Application AMI from Golden AMI
    stage ('Build AMI') {
        bat "packer build -var AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} -var AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} ${packerDir}/${packerTemplateFile}"
    }
    }
    }
    
}