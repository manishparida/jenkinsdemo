node('master') {
    stage('Initiating') {
        echo "Cleaning up workspace"
        deleteDir()
        WORKSPACE = pwd()
        echo "Preparing Environment"
    }
    stage('Checkout') {
        git_checkout()
    }
        if (terraformIncludeComputeALL == 'true') {
            withCredentials([[$class           : 'AmazonWebServicesCredentialsBinding',
                          credentialsId    : 'AWSCred',
                          accessKeyVariable: 'AWS_ACCESS_KEY_ID',
                          secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
        stage('ALL Remote State') {
            dir('stacks/frontend/compute/alb') {

                key = "app_alb"
                terraform_init(key, terraformBucket, terraformKey, terraformenv)
            }
        }

        stage('ALL Plan') {
            dir('stacks/frontend/compute/alb') {

                global_tfvars = "../../../../environments/global.tfvars"
                env_tfvars = "../../../../environments/${terraformenv}.tfvars"
                terraform_plan(global_tfvars, env_tfvars)
            }
        }
        if (terraformApplyPlan == 'true') {
            stage('ALL Apply') {
                dir('stacks/frontend/compute/alb') {

                    global_tfvars = "../../../../environments/global.tfvars"
                    env_tfvars = "../../../../environments/${terraformenv}.tfvars"
                    terraform_apply(global_tfvars, env_tfvars)
                }
            }
        }
    }
 }
 }

def git_checkout() {
        checkout([$class: 'GitSCM', branches: [[name: gitBranch]], clearWorkspace: true, doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'SubmoduleOption', disableSubmodules: false, parentCredentials: true, recursiveSubmodules: true, reference: '', trackingSubmodules: false]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: gitCreds, url: gitUrl]]])
    }

def terraform_init(key,terraformBucket,terraformKey,terraformenv) {
    bat 'terraform --version'
    bat "terraform init -input=false -backend=true -backend-config=bucket=${terraformBucket} -backend-config=key=${terraformKey}/${terraformenv}/${key}.tfstate -backend-config=region=ap-south-1"
}

def terraform_plan(global_tfvars,env_tfvars) {
        bat "terraform plan -no-color -out=tfplan -input=false -var-file=${global_tfvars} -var-file=${env_tfvars}"
}

def terraform_apply(global_tfvars,env_tfvars) {
        bat "terraform apply -input=false -no-color tfplan"
}