import groovy.json.JsonSlurper

node('master') {


    stage('Initiating') {
        echo "Cleaning up workspace"
        deleteDir()
        WORKSPACE = pwd()
        echo "Preparing Environment"
    }
    stage('Checkout') {
        git_checkout()
    }
    stage('Build') {
        withCredentials([[$class           : 'AmazonWebServicesCredentialsBinding',
                          credentialsId    : 'awscreds',
                          accessKeyVariable: 'AWS_ACCESS_KEY_ID',
                          secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {

            if (instance != "false") {
                dir('instance') {
                    if (instance == 'true' && terraformApplyPlan == 'true') {
                        bat "terraform init -no-color -force-copy -input=false -upgrade=true"
                        bat "terraform plan -out tfplan"
                        bat "terraform apply -input=false -no-color tfplan"
                    } else {
                        bat "terraform init -no-color -force-copy -input=false -upgrade=true"
                        bat "terraform plan -out tfplan"
                    }
                }
            }
            if (sg != "false") {
                dir('sg') {
                    if (sg == 'true' && terraformApplyPlan == 'true') {
                        bat "terraform init -no-color -force-copy -input=false -upgrade=true"
                        bat "terraform plan -out tfplan"
                        bat "terraform apply -input=false -no-color tfplan"
                    } else {
                        bat "terraform init -no-color -force-copy -input=false -upgrade=true"
                        bat "terraform plan -out tfplan"
                    }
                }
            }
        }
    }
}
    def git_checkout() {
        checkout([$class: 'GitSCM', branches: [[name: gitBranch]], clearWorkspace: true, doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'SubmoduleOption', disableSubmodules: false, parentCredentials: true, recursiveSubmodules: true, reference: '', trackingSubmodules: false]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: gitCreds, url: gitUrl]]])
    }
