node ('master') {
    def terraformdir_network      = "stacks/shared/network"
    def terraformdir_storage      = "stacks/shared/storage"
    def terraformdir_sns          = "stacks/shared/sns"

    dir(terraformenv) {
        stage ('Checkout') {
            git_checkout()
        }
        
        //Network module will run
        if (terraformIncludeNetwork == 'true') {
             withCredentials([[$class           : 'AmazonWebServicesCredentialsBinding',
                          credentialsId    : 'AWSCred',
                          accessKeyVariable: 'AWS_ACCESS_KEY_ID',
                          secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
            dir(terraformdir_network) {
                    terraformKey = "app_network.tfstate"
                    terraform_init(terraformBucket, terraformPrefix, terraformKey)
                }

                stage ('Network Stack Plan') {
                    global_tfvars = "../../../environments/global.tfvars"
                    env_tfvars = "../../../environments/${terraformenv}.tfvars"
                    terraform_plan(terraformenv, global_tfvars, env_tfvars)
                }
                if (terraformApplyPlan == 'true') {
                    stage ('Network Stack Apply') {
                        terraform_apply()
                    }
                }
            }
        }
// SNS module will run
        if (terraformIncludeSNS == 'true') {
             withCredentials([[$class           : 'AmazonWebServicesCredentialsBinding',
                          credentialsId    : 'AWSCred',
                          accessKeyVariable: 'AWS_ACCESS_KEY_ID',
                          secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
            dir(terraformdir_sns) {
                stage ('SNS Stack Remote State'){
                    terraformKey = "app_sns.tfstate"
                    terraform_init(terraformBucket, terraformPrefix, terraformKey)
                }
                stage ('SNS Stack Plan'){
                    global_tfvars = "../../../environments/global.tfvars"
                    env_tfvars = "../../../environments/${terraformenv}.tfvars"
                    terraform_plan(terraformenv, global_tfvars, env_tfvars)
                }
                if (terraformApplyPlan == 'true') {
                    stage ('SNS Stack Apply'){
                        terraform_apply()
                    }
                }
            }
        }
        }
// Storage module will run
        if (terraformIncludeStorage == 'true') {
            withCredentials([[$class           : 'AmazonWebServicesCredentialsBinding',
                          credentialsId    : 'AWSCred',
                          accessKeyVariable: 'AWS_ACCESS_KEY_ID',
                          secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
            dir(terraformdir_storage) {
                stage ('App Storage Stack Remote State'){
                    terraformKey = "app_storage.tfstate"
                    terraform_init(terraformBucket, terraformPrefix, terraformKey)
                }
                stage ('App Storage Stack Plan'){
                    global_tfvars = "../../../environments/global.tfvars"
                    env_tfvars = "../../../environments/${terraformenv}.tfvars"
                    terraform_plan(terraformenv, global_tfvars, env_tfvars)
                }
                if (terraformApplyPlan == 'true') {
                    stage ('App Storage Stack Apply'){
                        terraform_apply()
                    }
                }
            }
        }
    }
 }
}
def git_checkout() {
    checkout([$class: 'GitSCM', branches: [[name: gitBranch]], clearWorkspace: true, doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'SubmoduleOption', disableSubmodules: false, parentCredentials: true, recursiveSubmodules: true, reference: '', trackingSubmodules: false]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: gitCreds, url: gitUrl]]])
}

def terraform_init(terraformBucket,terraformPrefix,terraformkey) {
            bat "terraform init -input=false -backend=true -backend-config=bucket=${terraformBucket} -backend-config=key=${terraformPrefix}/${terraformkey} -backend-config=region=ap-south-1"
            bat "terraform get -no-color -update=true"
        }

def terraform_plan(workspace,global_tfvars,env_tfvars) {
    bat "terraform workspace select ${workspace}|| terraform workspace new ${workspace}"

    bat "terraform plan -no-color -out=tfplan -input=false -var-file=${global_tfvars} -var-file=${env_tfvars}"
    }

def terraform_apply() {
    bat "terraform apply -input=false -no-color tfplan"
}